
## 1. Getting Started with Ansible
- Overview on Ansible
- Why use Ansible

 

## 2. Ansible Architecture
- Control node
- Managed nodes
- Inventory
- Modules
- Tasks
- Playbooks
- Lab Environment

 

## 3. Pre-requisites to setup ansible
- Control node requirements
- Managed node requirements
- Update /etc/hosts on all the hosts
- Install mandatory pre-requisites on ansible client nodes

 

## 4. Installing Ansible
- Install using package manager on RHEL 8
- Install using package manager on CentOS 8
- Install using pip

 

## 5. Configuring Ansible
- Create normal user
- Configure password less authentication
- Verify password less SSH authentication
- Configure privilege escalation using sudo
- Verify ansible connectivity

 

## 6. Ansible configuration file (ansible.cfg)
- [defaults] section
- [ssh_connection] section
- [persistent_connection] section
- [colors] section

 

## 7. Using Ansible ad-hoc commands
- Overview
- Sample ad-hoc command examples
- How ansible works with modules
- Control the number of hosts for parallel execution (forks)
- Transfer file from Ansible Engine to Managed Nodes
- Download file from managed nodes to controller node
- Copy files locally on the remote server (managed node)
- Create or Remove file and directory
- Remove a file from the managed nodes
- Execute commands with root privileges
- Working with packages using yum module
- Execute ad-hoc commands as different user

 

## 8. Ansible inventory files
- Dynamic inventory
- Static inventory
- Provide hosts as an input argument
- Groups in an inventory file
- Groups of groups
- Regular expressions with an inventory file
- Variables in inventory

 

## 9. Working with managed nodes without python
 

## 10. Working with managed nodes with password (not passphrase)
 

## 11. Jinja2 templates and syntax
- Variables
- Use built-in filters
- Configure VSFTPD using Jinja2 templates

 

## 12. Ansible facts
- System default facts
- User defined facts

 

## 13. Ansible variables and data types
- Creating valid variable names
- Built-in variables
- Defining variables in inventory
- Defining variable in project
- Defining variables in playbook
- Defining variables using command line
- Accessing Variables
- Using register module to store output of any command
- Using set_fact module to create a new variable
- Prompt for user input with vars_prompt
- Read and access variables from separate YAML or JSON file
- Precedence

 

## 14. YAML syntax in ansible playbooks
- What is YAML?
- YAML file formatting
- Create or Modify .vimrc
- Constructing your ansible playbook

 

## 15. Introduction to ansible playbooks
- What are Ansible Playbooks
- Example-1: Your first playbook to install single package
- Example-2: Install multiple packages on different managed nodes
- Example-3: Disable gathering facts module
- Example-4: Assign custom name to the play and tasks
- Example-5: Execute playbook as shell scripts
- Example-6: Print debug message with playbooks
- Example-7: Increase verbosity level of playbook
- Example-8: Perform syntax check
- Example-9: Perform dry run of playbooks

 

## 16. Use Visual Code Studio to write playbooks (GUI)
- Download Visual Studio Code Repo
- Access Visual Code Studio
- Install Ansible Extension
- Configure Visual Studio to use Ansible
- Create playbook using Visual Studio

 

## 17. Using operators in Ansible
- Arithmetic Operators
- Comparison Operators
- Test operators
- Logical Operators
 

## 18. Ansible conditional statements
- Using when statement
- Using failed_when statement
- Print message with fail
- Using changed_when statement

 

## 19. Ansible handlers
- Handlers example
- Flush handlers and control when handler runs
- Using variables with handlers

 

## 20. Repeating tasks with Ansible loop
- Iterating over a simple loop
- Register output to a variable
- Iterating over list of hashes
- Iterate over dictionary

 

## 21. Using Ansible tags
- Understanding Tags
- Example-1: Add tags to all the tasks
- Example-2: Exclude tasks using tags
- Example-3: Using same tag for multiple tasks
- Example-4: Disable one or more tasks using tags

 

## 22. Ansible block and rescue (error recovery)
- Example-1: Why we should use blocks
- Example-2: Perform error recovery with rescue block
- Example-3: Practical example of error and recovery with blocks

 

## 23. Working with include and import module in Ansible
- include module
- include_tasks module
- import_tasks and import_playbook module

 

## 24. Ansible roles
- What are Ansible roles?
- Understanding ansible roles directory structure
- How variables should be defined in a role?
- Different location for Ansible roles

 

## 25. Create your first Ansible role
- Create project
- Example 1: Ansible roles example to update /etc/motd
- Example 2: Configure Virtual Hosting with Ansible Role

 

## 26. Ansible vaults
- Ansible vault encrypt file
- Ansible vault view encrypted files
- Ansible vault edit encrypted files
- Use ansible vault password file
- Ansible vault example to encrypt existing file
- Ansible vault change password of encrypted files
- Ansible vault decrypt file
- Using ansible vault in playbook
- Using Ansible playbook with vault password file
- Ansible vault encrypt string
